﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HW4.Models;

namespace HW4.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "Zroxx", Description="Orc Shaman heal level 120.", Class="Shaman" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Dupok", Description="Human Death Knight Tank level 120.", Class="Death Knight" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Drodos", Description="Druid DPS level 120.", Class="Druid" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "trogua", Description="Troll Rogue level 120.", Class="Rogue" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Unpriest", Description="Undead Priest Heal level 120.", Class="Priest" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Worra", Description="Tauren Warrior DPS level 120.", Class="Warrior" }
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}