﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HW4.Services;
using HW4.Views;

namespace HW4
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
