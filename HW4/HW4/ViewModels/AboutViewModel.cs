﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace HW4.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About the addon";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://www.curseforge.com/wow/addons/wowchallenger"));
        }

        public ICommand OpenWebCommand { get; }
    }
}